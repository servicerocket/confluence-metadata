/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.metadata.xstream;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import org.dom4j.Element;
import org.dom4j.DocumentFactory;
import org.dom4j.Attribute;
import org.dom4j.Node;
import org.dom4j.Text;
import org.randombits.confluence.metadata.Placeholder;

import java.util.Iterator;

/**
 * Converts the contents of the unknown object to a Placeholder which will save
 * the data back to the XML stream unchanged.
 * 
 * @author David Peterson
 */
public class PlaceholderConverter implements Converter {
    @SuppressWarnings("unchecked")
    public boolean canConvert( Class aClass ) {
        return Placeholder.class.equals( aClass );
    }

    public void marshal( Object object, HierarchicalStreamWriter writer, MarshallingContext context ) {
        Placeholder unknown = ( Placeholder ) object;
        Element root = unknown.getElement();

        writeElement( root, writer );
    }

    private void writeElement( Element root, HierarchicalStreamWriter writer ) {
        Iterator<Attribute> i = root.attributeIterator();
        while ( i.hasNext() ) {
            Attribute a = i.next();
            writer.addAttribute( a.getName(), a.getValue() );
        }

        i = root.nodeIterator();
        while ( i.hasNext() ) {
            Node n = ( Node ) i.next();

            if ( n instanceof Text ) {
                Text text = ( Text ) n;
                writer.setValue( text.getText() );
            } else if ( n instanceof Element ) {
                Element e = ( Element ) n;
                writer.startNode( e.getName() );
                writeElement( e, writer );
                writer.endNode();
            }
        }
    }

    public Object unmarshal( HierarchicalStreamReader reader, UnmarshallingContext context ) {
        DocumentFactory factory = DocumentFactory.getInstance();

        String name = reader.getNodeName();
        Element root = readElement( factory, reader );

        return new Placeholder( name, root );
    }

    private Element readElement( DocumentFactory factory, HierarchicalStreamReader reader ) {
        Element node = factory.createElement( reader.getNodeName() );

        // TODO: Figure out how to read the attachment name list. Perhaps a
        // custom extension of the reader?
        // Add attachments
        /**
         * Iterator i = reader.getAttributeNames(); while (i.hasNext()) { String
         * name = (String)i.next(); node.addAttribute(name,
         * reader.getAttribute(name)); }
         */

        while ( reader.hasMoreChildren() ) {
            // Check if there is any text content in this section.
            String value = reader.getValue();
            if ( value != null )
                node.add( factory.createText( value ) );

            reader.moveDown();

            node.add( readElement( factory, reader ) );

            reader.moveUp();
        }

        // Add any text content at the end of the section.
        String value = reader.getValue();
        if ( value != null )
            node.add( factory.createText( value ) );

        return node;
    }
}
