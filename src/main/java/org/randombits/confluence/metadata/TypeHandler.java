package org.randombits.confluence.metadata;

/**
 * Assists in converting Objects in to storable representations of those objects, and vice-versa.
 */
public interface TypeHandler {

    /**
     * Checks if this handler supports the specified value.
     *
     * @param original The value to check for.
     * @return <code>true</code> if it is supported.
     */
    boolean supportsOriginal( Object original );

    /**
     * Checks if this handler supports the specified {@link org.randombits.confluence.metadata.reference.Reference} instance.
     *
     * @param stored The stored instance.
     * @return <code>true</code> if the reference instance is supported.
     */
    boolean supportsStorable( Object stored );

    /**
     * Returns the original value for the specified reference object. This may
     * not be the same instance as the original value converted via {@link #getStorable(Object)},
     * but will be equal.
     *
     * @param stored The stored instance
     * @return The value of the reference object.
     * @throws ClassCastException if the reference is not supported.
     * @throws TypeConversionException if there is an issue.
     */
    Object getOriginal( Object stored ) throws TypeConversionException;

    /**
     * Returns a {@link org.randombits.confluence.metadata.reference.Reference} instance for the value.
     *
     * @param value The value to convert.
     * @return A Reference instance.
     * @throws ClassCastException if the value is not supported.
     * @throws TypeConversionException if there is an issue.
     */
    Object getStorable( Object value ) throws TypeConversionException;
}
