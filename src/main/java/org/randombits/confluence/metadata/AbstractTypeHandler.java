package org.randombits.confluence.metadata;

import org.apache.log4j.Logger;
import org.randombits.storage.Aliasable;

import static org.apache.log4j.Level.WARN;

/**
 * Provides basic checks for a specific reference type (S) and value type (O).
 *
 * @param <S> The reference type.
 * @param <O> The value that it references.
 */
public abstract class AbstractTypeHandler<O, S> implements TypeHandler, HasAlias {

    private static final Logger log = Logger.getLogger(AbstractTypeHandler.class);

    private final Class<O> originalType;

    private final Class<S> storedType;

    public AbstractTypeHandler( Class<O> originalType, Class<S> storedType ) {
        this.storedType = storedType;
        this.originalType = originalType;
    }

    protected Class<S> getStoredType() {
        return storedType;
    }

    protected Class<O> getOriginalType() {
        return originalType;
    }

    public boolean supportsOriginal( Object original ) {
        return supportsType( original );
    }

    public boolean supportsStorable( Object stored ) {
        return supportsType( stored );
    }

    private boolean supportsType( Object value ) {

        if ( originalType.isInstance( value ) || storedType.isInstance( value ) ) {
            return true;
        }

        if ( null != value ) {
            if ( value.getClass().getName().equals(storedType.getName()) ) {
                if (log.isEnabledFor(WARN)) {
                    log.warn( "Types of the same name (" + storedType.getName() + ") exist but version mismatched! Restarting Confluence or reinstalling this plugin might solve the problem.");
                }
            }
        }

        return false;
    }

    public Object getOriginal( Object stored ) {
        if ( originalType.isInstance( stored ) )
            return stored;
        return doGetOriginal( storedType.cast( stored ) );
    }

    protected abstract O doGetOriginal( S stored );

    public Object getStorable( Object original ) {
        // Check if it's already 'storable'.
        if ( storedType.isInstance( original ) )
            return original;

        return doGetStored( originalType.cast( original ) );
    }

    protected abstract S doGetStored( O original );

    public void applyAliases( Aliasable aliasable ) {
        aliasable.addAlias( getAlias(), storedType );
    }

    protected abstract String getAlias();
}
