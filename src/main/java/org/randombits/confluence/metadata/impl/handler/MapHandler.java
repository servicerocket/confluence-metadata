package org.randombits.confluence.metadata.impl.handler;

import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.TypeConversionException;
import org.randombits.confluence.metadata.TypeHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles converting objects stored inside Maps to and from 'storable' instances.
 */
public class MapHandler implements TypeHandler {

    private final MetadataManager metadataManager;

    public MapHandler(MetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    public boolean supportsOriginal( Object original ) {
        return original instanceof Map;
    }

    public boolean supportsStorable( Object stored ) {
        return stored instanceof Map;
    }

    public Object getOriginal( Object stored ) throws TypeConversionException {
        Map<?, ?> storedMap = (Map<?, ?>) stored;
        Map<Object, Object> originalMap = new HashMap<Object, Object>();
        for ( Map.Entry<?, ?> entry : storedMap.entrySet() ) {
            originalMap.put( metadataManager.fromStorable( entry.getKey() ), metadataManager.fromStorable( entry.getValue() ) );
        }

        return originalMap;
    }

    public Object getStorable( Object original ) throws TypeConversionException {

        Map<?, ?> originalMap = (Map<?, ?>) original;
        Map<Object, Object> storableMap = new HashMap<Object, Object>();
        for ( Map.Entry<?, ?> entry : originalMap.entrySet() ) {
            storableMap.put( metadataManager.toStorable( entry.getKey() ), metadataManager.toStorable( entry.getValue() ) );
        }

        return storableMap;
    }
}
