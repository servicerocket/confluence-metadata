package org.randombits.confluence.metadata.impl.handler;

import com.atlassian.confluence.core.ContentEntityManager;
import org.apache.log4j.Logger;
import org.randombits.confluence.metadata.AbstractTypeHandler;
import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.type.EvaluatedNumber;
import org.randombits.confluence.metadata.reference.EvaluatedNumberReference;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * Converts an {@link org.randombits.confluence.metadata.reference.EvaluatedNumberReference} into a {@link Number}.
 */
public class EvaluatedNumberHandler extends AbstractTypeHandler<EvaluatedNumber, EvaluatedNumberReference> {

    public static final String ALIAS = "ExpressionReference";

    private static final Logger LOG = Logger.getLogger(EvaluatedNumberHandler.class);

    private final ContentEntityManager contentEntityManager;

    private final MetadataManager metadataManager;

    public EvaluatedNumberHandler( @Qualifier("contentEntityManager") ContentEntityManager contentEntityManager, MetadataManager metadataManager) {
        super( EvaluatedNumber.class, EvaluatedNumberReference.class );
        this.contentEntityManager = contentEntityManager;
        this.metadataManager = metadataManager;
    }

    @Override
    protected EvaluatedNumber doGetOriginal( EvaluatedNumberReference stored ) {
        return new EvaluatedNumber(metadataManager, contentEntityManager.getById( stored.getContentId() ), stored.getDataPath(), stored.getExpression());
    }

    protected EvaluatedNumberReference doGetStored( EvaluatedNumber original ) {
        return new EvaluatedNumberReference( original.getContent().getId(), original.getDataPath(), original.getExpression() );
    }

    @Override
    protected String getAlias() {
        return ALIAS;
    }
}
