package org.randombits.confluence.metadata.impl.handler;

import org.randombits.confluence.metadata.HasAlias;
import org.randombits.confluence.metadata.TypeHandler;
import org.randombits.confluence.metadata.reference.WikiReference;
import org.randombits.storage.Aliasable;

/**
 * Handles {@link org.randombits.confluence.metadata.reference.WikiReference} instances.
 */
@SuppressWarnings({"deprecation"})
public class WikiReferenceHandler implements TypeHandler, HasAlias {

    public static final String LEGACY_ALIAS = "ListOption";

    public static final String ALIAS = "WikiReference";

    public boolean supportsOriginal( Object original ) {
        // This is a one-way handler, for backwards-compatibility.
        return false;
    }

    public boolean supportsStorable( Object stored ) {
        return stored instanceof WikiReference;
    }

    public String getOriginal( Object reference ) {
        WikiReference wikiReference = (WikiReference) reference;
        return wikiReference.getNonWiki();
        //return wikiReference.getWiki() == null ? wikiReference.getNonWiki() : wikiReference.getWiki();
    }

    public Object getStorable( Object value ) {
        // No creation of WikiReference supported.
        return null;
    }

    public void applyAliases( Aliasable aliasable ) {
        aliasable.addAlias( ALIAS, WikiReference.class );
    }
}
