package org.randombits.confluence.metadata.impl.handler;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UnknownUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.User;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.randombits.confluence.metadata.AbstractTypeHandler;
import org.randombits.confluence.metadata.reference.UserReference;

import static java.lang.String.format;

/**
 * Converts a {@link User} to a {@link org.randombits.confluence.metadata.reference.UserReference} and vice versa.
 */
public class UserHandler extends AbstractTypeHandler<User, UserReference> {

    private static final Logger log = Logger.getLogger(UserHandler.class);

    public static final String LEGACY_ALIAS = "UserOption";

    public static final String ALIAS = "UserReference";

    private final UserAccessor userAccessor;

    public UserHandler( UserAccessor userAccessor ) {
        super( User.class, UserReference.class );
        this.userAccessor = userAccessor;
    }

    @Override
    protected User doGetOriginal( UserReference stored ) {

        if ( null == stored.getUserKey() ) {

            // Reading username from UserReference should be avoided. This happens only if the data was unsuccessfully migrated,
            // likely because the user was/is missing.
            @SuppressWarnings("deprecation") String username = stored.getUsername();

            if ( null != stored.getUsername() ) {
                User user = userAccessor.getUserByName( username );
                if ( null != user ) {
                    return user;
                } else {
                    if ( log.isEnabledFor( Level.WARN ) ) {
                        log.warn( format( "Could not construct User '%s' as it is missing from the persistence. Recreate user in Confluence (then disable if necessary) to avoid reading this message again.", stored.getUsername() ) );
                    }
                    return new UnknownUser( stored.getUsername() );
                }
            }
            return null;
        } else {
            // User can actually be a deleted user here (ALA data stored in XML uses key not username). The `user_mapping` table helps to prevent a null.
            return userAccessor.getUserByKey(new UserKey(stored.getUserKey()));
        }
    }

    @Override
    protected UserReference doGetStored( User original ) {

        if ( original instanceof UnknownUser ) {
            // Constructing UserReference by name should only happen if the user was deleted and not migrated.
            @SuppressWarnings("deprecation") UserReference r = new UserReference( original.getName() );
            return r;
        } else if ( original instanceof ConfluenceUser) {
            return new UserReference( userAccessor.getUserByKey(((ConfluenceUser) original).getKey()) );
        } else {
            return new UserReference( userAccessor.getUserByName(original.getName()) );
        }
    }

    @Override
    protected String getAlias() {
        return ALIAS;
    }
}
