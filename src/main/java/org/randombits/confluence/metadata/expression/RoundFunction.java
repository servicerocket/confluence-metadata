/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.confluence.metadata.expression;

import org.randombits.math.eval.Cases;
import org.randombits.math.eval.Function;
import org.randombits.math.eval.Variable;
import org.randombits.math.eval.functions.FunctionParserExtension;

/**
 * Rounds the first parameter to the number of decimal places specified in the
 * second. Eg. 'round(12.3456, 2)==12.35' will evaluate to 'true'.
 * 
 * @author David Peterson
 * @version 1.0
 */
public class RoundFunction extends FunctionParserExtension {

    private static final long serialVersionUID = 2336669910297123899L;

    private static final String ROUND_NAME = "round";

    /**
     * Constructs a new RoundFunction.
     */
    public RoundFunction() {
        setName( ROUND_NAME );
    }

    public int getArity() {
        return 2;
    }

    public double getVal( double[] arguments ) {
        double newVal = arguments[0];
        long decs = ( long ) arguments[1];
        double xFactor = Math.pow( 10.0, decs );

        newVal = ( double ) Math.round( newVal * xFactor ) / xFactor;
        return newVal;
    }

    public double getValueWithCases( double[] arguments, Cases cases ) {
        // I have no idea what cases are for, so I'm ignoring them...
        return getVal( arguments );
    }

    public Function derivative( int wrt ) {
        return new RoundFunction();
    }

    public Function derivative( Variable x ) {
        return new RoundFunction();
    }

    public boolean dependsOn( Variable x ) {
        return false;
    }

}
