package org.randombits.confluence.metadata.reference;

import com.atlassian.confluence.labels.Label;

public class LabelReference implements Reference<Label> {

    private static final long serialVersionUID = 3987305789829134560L;

    private String labelName;
    
    // for backward compatibility. 
    private long contentId;  

    /**
     * Only used by XStream.
     */
    @SuppressWarnings({"UnusedDeclaration"})
    LabelReference() {
    }

    public LabelReference( Label label ) {
        this.labelName = label.getName();
        
    }

    public String getLabelName() {
        return labelName;
    }

    @Override
    public boolean equals( Object object ) {
        if ( object instanceof LabelReference ) {
            LabelReference ref = ( LabelReference ) object;
            return ref.labelName.equals( labelName );
        }
        return false;
    }

    @Override
    public int hashCode() {
        return labelName.hashCode() - 17;
    }
}
