package org.randombits.confluence.metadata.reference;

/**
 * The common parent class for all {@link Reference}s. This allows
 * implementations to specify a Value (<code>V</code>) type the reference
 * will return, as well as a Sort (<code>S</code>) type for sorting.
 * 
 * @param <T>
 *            The value type returned by the reference.
 */
public abstract class AbstractReference<T> implements Reference<T> {
}