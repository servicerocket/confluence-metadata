/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on 16/05/2005 by David Peterson
 */
package org.randombits.confluence.metadata.reference;

import org.apache.commons.lang.StringUtils;

/**
 * A simple list item.
 * 
 * @author David Peterson
 * @version 1.0
 * @deprecated Use direct String values now.
 */
@Deprecated
public class WikiReference implements Reference {

    private static final long serialVersionUID = 8080712643773587825L;

    private String nonWiki;

    private String wiki;

    protected String sortValue;

    /**
     * Public default constructor for the simple item.
     */
    @SuppressWarnings({"UnusedDeclaration"})
    WikiReference() {
    }

    /**
     * @param nonWiki
     *            The non-wiki version.
     * @param wiki
     *            The wiki-version.
     */
    public WikiReference( String nonWiki, String wiki ) {
        this( nonWiki, wiki, wiki );
    }

    public WikiReference( final String nonWiki, final String wiki, final String sortValue ) {
        this.nonWiki = nonWiki;
        this.wiki = wiki;
        this.sortValue = sortValue;
    }

    public String getReferred() {
        return wiki;
    }

    /**
     * Tests if the two objects are equal.
     * 
     * @param obj
     *            the object to test.
     * @return <code>true</code> if the object is a SimpleItem with the same
     *         values.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override public boolean equals( Object obj ) {
        if ( obj instanceof WikiReference ) {
            WikiReference si = ( WikiReference ) obj;
            return StringUtils.equals( wiki, si.wiki ) && StringUtils.equals( nonWiki, si.nonWiki );
        }
        return false;
    }

    /**
     * @return the hash code for the list item.
     * @see java.lang.Object#hashCode()
     */
    @Override public int hashCode() {
        int code = 23;
        if ( wiki != null )
            code += wiki.hashCode();
        if ( nonWiki != null )
            code += nonWiki.hashCode();
        return code;
    }

    /**
     * @return the nonWiki values.
     * @see java.lang.Object#toString()
     */
    @Override public String toString() {
        return nonWiki;
    }

    /**
     * @return Returns the nonWiki.
     */
    public String getNonWiki() {
        return nonWiki;
    }

    /**
     * @return Returns the wiki.
     */
    public String getWiki() {
        return wiki;
    }
}
