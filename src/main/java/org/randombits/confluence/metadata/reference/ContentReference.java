/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on 16/05/2005 by David Peterson
 */
package org.randombits.confluence.metadata.reference;

import com.atlassian.confluence.core.ContentEntityObject;

/**
 * An implementation of
 * {@link Reference}.
 * 
 * @author David Peterson
 * @version 1.0
 */
public class ContentReference implements Reference<ContentEntityObject> {

    private static final long serialVersionUID = -4799535717438558134L;

    private long id;

    /**
     * Constructs a new ContentItem. Should only be used by xStream.
     */
    public ContentReference() {
    }

    /**
     * Constructs a new ContentItem with the specified entity.
     * 
     * @param content
     *            The content.
     */
    public ContentReference( ContentEntityObject content ) {
        id = content.getId();
    }

    /**
     * @return The current ContentEntity's ID.
     */
    public long getId() {
        return id;
    }

    /**
     * Checks if the specified object is equal to this one.
     * 
     * @param obj
     *            The object to test.
     * @return <code>true</code> if the object is a ContentItem and its id is
     *         equal to this object's id.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override public boolean equals( Object obj ) {
        if ( obj instanceof ContentReference ) {
            ContentReference ci = ( ContentReference ) obj;

            return id == ci.id;
        }
        return false;
    }

    /**
     * @return the hash code for the list option.
     * @see java.lang.Object#hashCode()
     */
    @Override public int hashCode() {
        return 23 + ( int ) id;
    }

    /**
     * @return the string values of the option.
     * @see java.lang.Object#toString()
     */
    @Override public String toString() {
        return Long.toString( id );
    }
}