package org.randombits.confluence.metadata;

public interface Notifiable {
    void storageNotification( String key );
    
    void removalNotification( String key );
}
